# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This Source Code Form is "Incompatible With Secondary Licenses", as
# defined by the Mozilla Public License, v. 2.0.

# This file contains a single hash named %strings, which is used by the
# installation code to display strings before Template-Toolkit can safely
# be loaded.
#
# Each string supports a very simple substitution system, where you can
# have variables named like ##this## and they'll be replaced by the string
# variable with that name.
#
# Please keep the strings in alphabetical order by their name.

%strings = (
    any => '任何',
    apachectl_failed => <<END,
警告：无法檢查 Apache 的设置。当你并非以 ##root## 执行
checksetup.pl 时，这有可能会发生。若要查看所遭遇到的问题，
执行： ##command##
END
    bad_executable => '并非有效的执行檔： ##bin##',
    blacklisted => '(被列入黑名單)',
    bz_schema_exists_before_220 => <<'END',
你正从 2.20 以前的版本升級，但是 bz_schema table 已经存在。
这代表你并沒有先 drop 已经存在的 Bugzilla 资料库，就把 mysqldump
回存到 Bugzilla 资料库。当你需要回存 Bugzilla 资料库的備份时，
你必须先 drop 整个资料库。

请 drop 你的 Bugzilla 资料库，并从一个不包含 bz_schema table 的備份回存。
如果因为某些原因你无法这样做，那么你可以使用最后的手段：连到你的 MySQL
资料库后， drop bz_schema table 。
END
    checking_for => '檢查',
    checking_dbd => '檢查可用的 perl DBD 模組...',
    checking_optional => '下列 perl 模組是选用的：',
    checking_modules => '檢查 perl 模組中...',
    chmod_failed => '##path##: 变更权限失敗： ##error##',
    chown_failed => '##path##: 变更拥有者失敗： ##error##',
    commands_dbd => <<EOT,
你必须执行以下指令之一（视你使用的资料库而定）：
EOT
    commands_optional => '安裝选用模組的指令：',
    commands_required => <<EOT,
安裝必備模組的指令（你「必须」执行所有的指令后，重新执行
checksetup.pl ）：
EOT
    continue_without_answers => <<'END',
在互动模式（不会有一个 'answers' 档案）重新执行 checksetup.pl
以继续。
END
    cpan_bugzilla_home =>
        "警告：使用 Bugzilla 的目錄做为 CPAN home 资料夾。",
    db_enum_setup  => "设置标准的下拉式栏位选项：",
    db_schema_init => "初始化 bz_schema...",
    db_table_new   => "增加新的 table ##table##...",
    db_table_setup => "建立 table 中...",
    done => '完成。',
    enter_or_ctrl_c => "Press Enter to continue or Ctrl-C to exit...",
    error_localconfig_read => <<'END',
讀取 ##localconfig## 档案时发生错误。错误的讯息是：

##error##

请在 localconfig 檔中修正此错误。
或者对 localconfig 檔更名，然后重新执行 checksetup.pl ，
让它重新建立新的 localconfig 档案：

  $ mv -f localconfig localconfig.old
  $ ./checksetup.pl
END
    extension_must_return_name => <<END,
##file## 回傳了 ##returned## ，但这不是个有效的附加元件名称。
附加元件必须要回傳它們的名称，而不是 <code>1</code> 或数字。
詳情请參閱 Bugzilla::Extension 。
END
    feature_auth_ldap => 'LDAP 认证',
    feature_auth_radius => 'RADIUS 认证',
    feature_documentation => '说明文件',
    feature_graphical_reports => '图表式报告',
    feature_html_desc => '在产品／群组描述中使用较多的 HTML',
    feature_inbound_email => 'Inbound Email',
    feature_jobqueue => '信件佇列',
    feature_jsonrpc => 'JSON-RPC 界面',
    feature_new_charts => '新图表',
    feature_old_charts => '旧图表',
    feature_memcached => '支援 Memcached',
    feature_mod_perl => 'mod_perl',
    feature_moving => '在不同机器间搬移 Bugs',
    feature_patch_viewer => 'Patch Viewer',
    feature_rest => 'REST 界面',
    feature_smtp_auth => 'SMTP 认证',
    feature_smtp_ssl => 'SMTP 的 SSL 支援',
    feature_updates => '自动更新通知',
    feature_xmlrpc => 'XML-RPC 界面',
    feature_detect_charset    => '自动侦测文字附件的字元編碼',
    feature_typesniffer       => '对附件的 MIME 类别做探測(sniff)',

    file_remove => '移除 ##name## 中...',
    file_rename => '将 ##from## 更名为 ##to## 中...',
    header => "* 这是在 ##os_name## ##os_ver## ，\n"
            . "* Perl ##perl_ver## 上执行的 Bugzilla ##bz_ver## 。",
    install_all => <<EOT,

如要試著使用單一指令自动安裝所有必備及选用的模組，执行：

  ##perl## install-module.pl --all

EOT
    install_data_too_long => <<EOT,
警告：在 ##table##.##column## 栏位中的某些资料比新的長度限制 ##max_length## 字元还要長。
需要修正的资料，以栏位名称 ##id_column## 以及需要修正的栏位值 ##column## 的格式，
列出如下：

EOT
    install_module => '安裝 ##module## 版本 ##version## 中...',
    installation_failed => '*** 安裝已中止。请阅读上述讯息。 ***',
    install_no_compiler => <<END,
错误：
错误：使用 install-module.pl 必须先安裝一种編譯器，例如
gcc 。
END
    install_no_make => <<END,
错误：使用 install-module.pl 必须先安裝 "make" 。
END
    lc_new_vars => <<'END',
此版本的 Buzilla 包含一些你也許想要改变及套用到本地设置的参数。
自从你上次执行 checksetup.pl 以后，以下参数对 ##localconfig##
而言是新的：

##new_vars##

请编辑 ##localconfig## 并重新执行 checksetup.pl 以完成安裝。
END
    lc_old_vars => <<'END',
##localconfig## 中的以下参数已不再使用，并已被搬到
##old_file## 中： ##vars##
END
    localconfig_create_htaccess => <<'END',
如果你使用 Apache 做为你的網頁伺服器， Bugzilla 可以替你建立
.htaccess 档案，它可以让此档案 (localconfig) 和其他的機密档案
无法透过网络讀取。

设置为 1 时， checksetup.pl 将会檢查是否有 .htaccess 档案。
若无，則会建立 .htaccess 档案。

设置为 0 时， checksetup.pl 将不会建立 .htaccess 档案。
END
    localconfig_db_check => <<'END',
checksetup.pl 是否应该尝试檢查你的资料库设置是否正确？
有些资料库伺服器/Perl 模組/moonphase 的組合会让此功能无法運作，
因此你可以尝试设置此参数为 0 ，让 checksetup.pl 可以执行。
END
    localconfig_db_driver => <<'END',
使用哪一种 SQL 资料库。预设值是 mysql 。
支援的资料库列表可以藉由列出 Bugzilla/DB 目錄维护 －－
每个模組都对应到一种支援的资料库，而模組的檔名（在 ".pm" 之前）
都对应到此参数的一个有效的值。
END
    localconfig_db_host => <<'END',
此资料库伺服器所在的 DNS hostname 或 IP 位址。
END
    localconfig_db_name => <<'END',
资料库的名称。对 Oracle 而言，是资料库的 SID 。对 SQLite 而言，
是 DB 档案的名称（或路徑）。
END
    localconfig_db_pass => <<'END',
在此输入资料库的密码。通常建议对你的 bugailla 资料库使用者指定密码。
如果你输入了含有縮写符号 (') 或反斜線 (\) 的密码，你需要使用 '\'
字元做 escape 。 (\') 或 (\)
（不用这些字元的话简单地多。）
END
    localconfig_db_port => <<'END',
有时资料库伺服器会使用非标准的 port 。如果有这种情況的话，在此参数指定该
port 的数字。设置为 0 代表「使用预设的 port 」。
END
    localconfig_db_sock => <<'END',
MySQL 才需要输入的：输入 unix socket for MySQL 的路徑。
如不输入，那么会使用 MySQL 的预设值。大概会是你要的。
END
    localconfig_db_user => "用來连接资料库的使用者",
    localconfig_db_mysql_ssl_ca_file => <<'END',
PEM 档案的路徑，此档案包含可信任的 SSL CA 發行者清单。
此档案必须是網頁伺服器使用者可以讀取的。
END
    localconfig_db_mysql_ssl_ca_path => <<'END',
资料夾的路徑，此资料夾包含以 PEM 格式储存的，可信任的 SSL CA 發行者清单。
资料夾及其底下的档案必须是網頁伺服器使用者可以讀取的。
Directory and files inside must be readable by the web server user.
END
    localconfig_db_mysql_ssl_client_cert => <<'END',
会使用於资料库伺服器的完整客戶端 SSL 档案路徑（ PEM 格式）。
此档案必须是網頁伺服器使用者可以讀取的。
END
    localconfig_db_mysql_ssl_client_key => <<'END',
相对於客戶端 SSL 憑證的私鑰完整路徑。
此档案必须是網頁伺服器使用者可以讀取的，且必须不被密码保護。
END
    localconfig_diffpath => <<'END',
如果要让「兩个 patch 之间的 diff 」功能有效，需要知道 "diff" bin
在哪个目錄。（只有在使用 Patch Viewer 功能时才需要设置。）
END
    localconfig_index_html => <<'END',
大部份的網頁伺服器允许你使用 index.cgi 做为目錄索引，并且也已经
事先设置好了。如果你的沒有，那么你会需要一个 index.html 档案，
以避免重導到 index.cgi 。设置 $index_html 为 1 时，
checksetup.pl 会在 index.html 不存在时，替你建立一个。
注意： checksetup.pl 不会取代已存在的档案，所以如果你希望
       checksetup.pl 替你建立 index.html 时，你必须先确认
       index.html 档案不存在。
END
    localconfig_interdiffbin => <<'END',
如果你想要使用 Patch Viewer 的「兩个 patch 之间的 diff 」功能，
请指定 "interdiff" 可执行的完整路徑。
END
    localconfig_site_wide_secret => <<'END',
此密鑰用來建立及驗證加密的 token 。这些 token 用在 Bugzilla 的
一些安全性相关的功能中，以保護 Bugzilla 避免特定的攻擊。
预设是一組亂数字串。把这組密鑰保管好是很重要的。
此字串也必须要很長。
END
    localconfig_use_suexec => <<'END',
如果 Bugzilla 在 Apache SuexecUserGroup 环境下执行，请设置为 1 。

如果你的網頁伺服器有使用 cPanel 、 Plesk 或者類似的控制軟體，
或是你的 Bugzilla 是在 shared hosting 的环境下，那你大概就是在
Apache SuexecUserGroup 环境下。

如果是 Windows box ，忽略此设置，因为它无效。

如果设置为 0 ， checksetup.pl 会设置正常網頁伺服器环境下，
適当的档案权限。

如果设置为 1 ， checksetup.p 会设置適合的档案权限，让 Bugzilla 在
SuexecUserGroup 环境下可以正常運作。
END
    localconfig_webservergroup => <<'END',
执行網頁伺服器的群组名称。在 Red Hat 下通常是 "apache" 。在
Debian/Ubuntu 下通常是 "www-data" 。

如果你把下面的 use_suexec 参数打开的话，那么是轉換过后的網頁伺服器的
的群组名称，以执行 cgi 档案。

如果是 Windows 上，忽略此设置，因为它无效。

如果你沒有权限可以存取 script 执行的目錄，设置为 "" 。
如果设置为 "" ，那么你所安裝的 Bugzilla 将会“非常”不安全，
因为有些档案将可以隨意讀取／写入，所以只要有权限進入机器的人
就可以隨便做什麼。你只应该在测试安裝，而且无法设置为其他值时，
才设置成 "" 。※※！！已经警告过你了！！※※

如果设置为 "" 以外的值，你必须要以该指定群组中的成员，或是
##root## ，才能执行 checksetup.pl 。
END
    max_allowed_packet => <<EOT,
警告：你必须在你的 MySQL 设置里将 max_allowed_packet 参数设置为
##needed## 。現在它设置为 ##current## 。
你可以在 MySQL 设置檔中的 [mysqld] 段落中找到此参数。
EOT
    min_version_required => "最低版本需求： ",

# Note: When translating these "modules" messages, don't change the formatting
# if possible, because there is hardcoded formatting in
# Bugzilla::Install::Requirements to match the box formatting.
    modules_message_apache => <<END,
***********************************************************************
* APACHE 模組                                                         *
***********************************************************************
* 有些 Apache 模組可以擴充 Bugzilla 的功能。                          *
* 这些模組可以在 Apache 的设置檔（通常叫 httpd.conf 或 apache2.conf ）*
* 中启用。                                                            *
* - mod_headers, mod_env, mod_expires:                                *
*   当升級 Bugzilla 时，它們可以自动更新使用者的浏览器快取。          *
* - mod_rewrite:                                                      *
*   可以写入由 REST API 使用的，较短的网址。                          *
* - mod_version:                                                      *
*   可以針对 Apache 2.2 或 2.4 写入規則至 .htaccess 档案中。          *
* 你需要启用的模組有：                                                *
*                                                                     *
END
    modules_message_db => <<EOT,
***********************************************************************
* 存取资料库                                                          *
***********************************************************************
* 为了要存取资料库， Bugzilla 需要在你所执行的资料库中安裝正确的      *
* "DBD" 模組。请在以下指令中找出正确的指令，以在资料库中安裝適当      *
* 的模組。                                                            *
EOT
    modules_message_optional => <<EOT,
***********************************************************************
* 选用的模組                                                          *
***********************************************************************
* 有些 Perl 模組并非安裝 Bugzilla 所必備，然而安裝最新版本的某些      *
* 模組可以让你拥有額外的功能。                                        *
*                                                                     *
* 你不一定要安裝的选用模組，以及它們所启用的功能名称列表如下。        *
* 在列表的下方則是安裝每一模組所使用的指令。                          *
EOT
    modules_message_required => <<EOT,
***********************************************************************
* 必備的模組                                                          *
***********************************************************************
* Bugzilla 要求你安裝某些 Perl 模組。原因可能是你并未安裝这些模組，   *
* 或是版本过旧，需要更新。下方是安裝这些模組所需要用的指令。          *
EOT

    module_found => "找到版本 v##ver##",
    module_not_found => "沒有找到",
    module_ok => 'OK',
    module_unknown_version => "找到未知的版本",
    mysql_innodb_disabled => <<'END',
你所安裝的 MySQL 已停用 InnoDB 。
Bugzilla 需要 InnoDB 启用才能使用。
请启用后重新执行 checksetup.pl 。
END
    mysql_index_renaming => <<'END',
即将对旧的索引进行更名。更名完成所需要的时间估計是
##minutes## 分钟。一旦开始就不能中斷。如果你想要取
消，请現在按 Ctrl-C ...
（等 45 秒 ...）
END
    mysql_utf8_conversion => <<'END',
警告：我們将把你所储存的资料格式轉成 UTF-8 。这将会允许 Bugzilla 正确地
      储存及排序國際字元。然而，如果你的资料库中有任何非 UTF-8 的资料，
      这些资料※※将会被刪除※※。所以，在你继续执行 checksetup.pl 前，
      如果你有任何非 UTF-8 的资料（或是你不确定），你应该馬上按 Ctrl-C
      中斷 checksetup.pl ，并执行 contrib/recode.pl 让所有在资料库中的
      资料轉成 UTF-8 。你在继续之前也应该先備份。将会影響资料库中的所有
      table ，即使它們和 Bugzilla 无关。

      如果你曾经使用过比 Bugzilla 2.22 版本旧的 Bugzilla ，我們※強烈※
      建议你※現在※停止 checksetup.pl 并执行 contrib/recode.pl 。
END
    no_checksetup_from_cgi => <<END,
<!DOCTYPE html>
<html>
  <head>
    <title>无法从網頁伺服器执行 checksetup.pl</title>
  </head>

  <body>
    <h1>无法从網頁伺服器执行 checksetup.pl</h1>
    <p>
      你<b>不能</b>从浏览器执行此程式。要安裝或升級
      Bugzilla ，从命令列（例如 Linux 上的
      <kbd>bash</kbd> 、 <kbd>ssh</kbd> 或 Windows 上的
      <kbd>cmd.exe</kbd> 执行此程式），并依照所給予的指示进行。
    </p>

    <p>
      如要获得更多关于如何安裝 Bugzilla 的资讯，请至官方
      Bugzilla 网站
      <a href="http://www.bugzilla.org/docs/">阅读文件</a>。
    </p>
  </body>
</html>
END
    patchutils_missing => <<'END',
額外选项備註：如果你想要使用 Bugzilla 的「兩个 patch 之间的 diff 」功能
（它需要 PatchReader 这个 Perl 模組），你应该从以下网址安裝 patchutils
：

    http://cyberelk.net/tim/software/patchutils/
END
    no_such_module => "在 CPAN 上找不到叫 ##module## 的 Perl 模組。",
    template_precompile   => "預先編譯模版中...",
    template_removal_failed => <<END,
警告：无法移除目錄 '##template_cache##' 。
      已将它更名为 '##deleteme##' 。如要釋放磁碟空间，
      请手动刪除。
END
    template_removing_dir => "刪除存在的已編譯模版中...",
    update_cf_invalid_name =>
        "移除自订栏位 '##field##' 中，因为它有无效的名称...",
    update_flags_bad_name => <<'END',
"##flag##" 不是有效的旗标名称。请把它更名为不含空白或逗号 (,)
的名称。
END
    update_nomail_bad => <<'END',
警告：以下使用者列在 ##data##/nomail 中，但是这里沒有账号。
不符的部份已移至 ##data##/nomail.bad 中：
END
    update_summary_truncate_comment =>
        "摘要栏位中的原始值長於 255 字元，因此在升級中被截斷了。"
        . "原本的摘要是：\n\n##summary##",
    update_summary_truncated => <<'END',
警告：你有一些 bugs 的摘要長於 255 字元。已将这些 bugs 的摘要截斷至
255 字元，而原本过長的摘要已複製一份至意见中。受到影響的 bug 编号是：
END
    update_quips => <<'END',
智語現在储存在资料库中，而不是存在外部档案。原先储存在
##data##/comments 的智語已被複製到资料库中，并将该档案更名为
##data##/comments.bak 。你只要确认智語有搬移成功，即可将该檔
案刪除。
END
    update_queries_to_tags => "填入新的 'tag' table 中：",
    webdot_bad_htaccess => <<END,
警告：依赖关系图表的图无法存取。
刪除 ##dir##/.htaccess 后重新执行 checksetup.pl 。
END
);

1;
